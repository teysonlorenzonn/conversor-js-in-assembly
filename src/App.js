import React from 'react';
import { CssBaseline, colors } from '@material-ui/core';
import { createTheme, ThemeProvider } from '@material-ui/core/styles';
import { BrowserRouter } from 'react-router-dom';
import Routes from './routes';
import Header from './components/Header';

const customTheme = createTheme({
  palette: {
    primary: {
      main: colors.lightBlue[500],
    },
    secondary: {
      main: colors.lightBlue[400],
    },
  },
  typography: {
    fontFamily: ['Helvetica Neue', 'Roboto'].join(','),
  },
});

const App = () => {
  return (
    <ThemeProvider theme={customTheme}>
      <CssBaseline>
        <Header />
        <BrowserRouter>
          <Routes />
        </BrowserRouter>
      </CssBaseline>
    </ThemeProvider>
  );
};

export default App;
