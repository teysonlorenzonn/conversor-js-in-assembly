import React, { useState } from 'react';
import { Grid, colors } from '@material-ui/core';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import { v4 as uuid } from 'uuid';
import Cards from '../../components/Cards';
import {
  expression0,
  expression1,
  expression2,
  expression3,
} from './codes/implementation';
import Blocks from '../../components/Blocks';

const itemsDrag = [
  {
    id: uuid(),
    content: expression0,
  },
  {
    id: uuid(),
    content: expression1,
  },
  {
    id: uuid(),
    content: expression2,
  },
  {
    id: uuid(),
    content: expression3,
  },
];

const columnsDrag = {
  codeJs: {
    name: 'Código JavaScript',
    items: itemsDrag,
  },
  convert: {
    name: 'Converter',
    items: [],
  },
};

const onDragEnd = (result, columns, setColumns) => {
  if (!result.destination) return;
  const { source, destination } = result;

  if (source.droppableId !== destination.droppableId) {
    const sourceColumn = columns[source.droppableId];
    const destColumn = columns[destination.droppableId];
    const sourceItems = [...sourceColumn.items];
    const destItems = [...destColumn.items];
    const [removed] = sourceItems.splice(source.index, 1);
    destItems.splice(destination.index, 0, removed);
    setColumns({
      ...columns,
      [source.droppableId]: {
        ...sourceColumn,
        items: sourceItems,
      },
      [destination.droppableId]: {
        ...destColumn,
        items: destItems,
      },
    });
  } else {
    const column = columns[source.droppableId];
    const copiedItems = [...column.items];
    const [removed] = copiedItems.splice(source.index, 1);
    copiedItems.splice(destination.index, 0, removed);
    setColumns({
      ...columns,
      [source.droppableId]: {
        ...column,
        items: copiedItems,
      },
    });
  }
};

const Home = () => {
  const [columns, setColumns] = useState(columnsDrag);

  return (
    <Grid container>
      <DragDropContext
        onDragEnd={(result) => onDragEnd(result, columns, setColumns)}
      >
        {Object.entries(columns).map(([id, column]) => (
          <Droppable droppableId={id} key={id}>
            {(provided, snapshot) => (
              <Grid
                item
                ref={provided.innerRef}
                xs={6}
                {...provided.droppableProps}
              >
                <Cards
                  title={column.name}
                  style={{
                    background: snapshot.isDraggingOver
                      ? colors.grey[200]
                      : 'white',
                    height: '98%',
                  }}
                >
                  {column.items.map((item, index) => (
                    <Draggable
                      key={item.id}
                      draggableId={item.id}
                      index={index}
                    >
                      {(provided, snapshot) => (
                        <Blocks
                          innerRef={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                          style={{
                            backgroundColor: snapshot.isDragging
                              ? colors.lightBlue[500]
                              : 'white',
                            ...provided.draggableProps.style,
                          }}
                          htmlText={item.content}
                          colorText={snapshot.isDragging ? 'white' : 'black'}
                        />
                      )}
                    </Draggable>
                  ))}
                  {provided.placeholder}
                </Cards>
              </Grid>
            )}
          </Droppable>
        ))}
      </DragDropContext>
      <Grid item xs={12}>
        <Cards title="Resultado">
          {columns.convert.items.map((item, idx) => (
            <Blocks key={idx} htmlText={item.content} />
          ))}
        </Cards>
      </Grid>
    </Grid>
  );
};

export default Home;
