export const expression0 = `<span>const a = 10;<br/>
const b = 5;<br/>
console.log('soma', a + b);</span>`;

export const expression1 = `<span>const a = 10;<br/>
const b = 5;<br/>
console.log('subtração', a - b);</span>`;

export const expression2 = `<span>const a = 10;<br/>
const b = 5;<br/>
console.log('multiplicação', a * b);</span>`;

export const expression3 = `<span>const a = 10;<br/>
const b = 5;<br/>
console.log('divisão', a / b);</span>`;
