import React from 'react';
import { makeStyles, Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  cardCode: {
    padding: theme.typography.pxToRem(16),
    margin: '0 0 5px 0',
    borderRadius: theme.typography.pxToRem(8),
    minHeight: theme.typography.pxToRem(50),
    color: 'white',
    border: '1px solid lightgrey',
  },
}));

const Blocks = ({ htmlText, colorText, innerRef, ...props }) => {
  const styles = useStyles();

  return (
    <div ref={innerRef} {...props} className={styles.cardCode}>
      <Typography
        dangerouslySetInnerHTML={{ __html: htmlText }}
        variant="subtitle1"
        style={{
          color: colorText ? colorText : 'black',
        }}
      />
    </div>
  );
};

export default Blocks;
