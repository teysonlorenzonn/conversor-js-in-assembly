import React from 'react';
import {
  Toolbar,
  Tooltip,
  Typography,
  AppBar,
  makeStyles,
  withStyles,
} from '@material-ui/core';
import SchoolIcon from '@material-ui/icons/School';
import InfoIcon from '@material-ui/icons/Info';
import ImportContactsIcon from '@material-ui/icons/ImportContacts';
import AssignmentReturnedIcon from '@material-ui/icons/AssignmentReturned';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginBottom: theme.typography.pxToRem(15),
  },
  menuText: {
    color: 'white',
    fontSize: theme.typography.pxToRem(25),
    fontWeight: '500',
  },
  appBar: {
    height: theme.typography.pxToRem(60),
    display: 'flex',
    justifyContent: 'center',
  },
  toolBar: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  infoButton: {
    color: 'white',
    cursor: 'pointer',
  },
  info: {
    padding: '10px',
    '& h6': {
      fontSize: theme.typography.pxToRem(25),
      fontWeight: '600',
    },
    '& div': {
      display: 'flex',
      justifyContent: 'flex-start',
      alignItems: 'center',
      paddingLeft: '8px',
      '& span': {
        fontSize: theme.typography.pxToRem(16),
        paddingLeft: '5px',
      },
    },
  },
  icons: {
    marginRight: '5px',
    cursor: 'pointer',
    color: 'white',
  },
}));

const HtmlTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: 'white',
    color: 'black',
    border: '1px solid #dadde9',
  },
  arrow: {
    color: 'white',
  },
}))(Tooltip);

const Header = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.appBar}>
        <Toolbar variant="dense" className={classes.toolBar}>
          <Typography variant="h6" className={classes.menuText}>
            JSAMY CONVERTER
          </Typography>
          <div>
            <a
              href={
                'https://bitbucket.org/teysonlorenzonn/conversor-js-in-assembly/get/master.zip'
              }
              download
            >
              <AssignmentReturnedIcon className={classes.icons} />
            </a>
            <HtmlTooltip title={<InfoContainer classes={classes} />} arrow>
              <InfoIcon className={classes.infoButton} />
            </HtmlTooltip>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
};

const InfoContainer = ({ classes }) => {
  return (
    <div className={classes.info}>
      <Typography variant="h6">Desenvolvedores:</Typography>
      <div>
        <SchoolIcon color="primary" />
        <span>Teyson Lorenzon</span>
      </div>
      <div>
        <SchoolIcon color="primary" />
        <span>Matheus Galvain</span>
      </div>
      <div>
        <SchoolIcon color="primary" />
        <span>Andersson Severo</span>
      </div>
      <div>
        <SchoolIcon color="primary" />
        <span>Maurício Ogliari</span>
      </div>
      <div>
        <SchoolIcon color="primary" />
        <span>Wesley dos Santos</span>
      </div>

      <Typography variant="h6">Orientador:</Typography>
      <div>
        <ImportContactsIcon color="primary" />
        <span>Marcos Lucas</span>
      </div>
    </div>
  );
};

export default Header;
