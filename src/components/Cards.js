import React from 'react';
import { string, node } from 'prop-types';
import { Card, CardContent, Typography, makeStyles } from '@material-ui/core';
import classNames from 'classnames';

const useStyles = makeStyles((theme) => ({
  card: {
    margin: theme.typography.pxToRem(5),
  },
  title: {
    textAlign: 'center',
    marginBottom: theme.typography.pxToRem(15),
  },
}));

const Cards = ({ title, className, children, ...props }) => {
  const styles = useStyles();

  return (
    <Card {...props} className={classNames(styles.card, className)}>
      <CardContent>
        {title && (
          <Typography className={styles.title} variant="h5" component="h2">
            {title}
          </Typography>
        )}
        {children}
      </CardContent>
    </Card>
  );
};

Cards.propTypes = {
  title: string,
  className: string,
  children: node,
};

export default Cards;
